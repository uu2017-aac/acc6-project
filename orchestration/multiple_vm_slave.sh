#!/bin/sh
echo "Setting up slave!"
# Key added by cloud-init and heat template
echo "Updating the packages..."
apt-get update -y
apt-get upgrade -y
echo "Installing needed packages..."
apt-get install -y python-pip octave
pip install celery
echo "System setup done!"
echo "Fetching the data"
mkdir -p /home/ubuntu/app
wget https://www.dropbox.com/s/nc7rskzcuhv9kmt/benchop_aas.tar.xz -O \
    /home/ubuntu/app/benchop_aas.tar.xz
echo "Preparing the code"
cd /home/ubuntu/app && tar xvf benchop_aas.tar.xz
echo "Fixing the code to connect to the master node"
sed 's/localhost/MASTER_IP/' -i /home/ubuntu/app/worker.py
#echo "Mounting the data volume"
#mkdir -p /home/ubuntu/app/data
#mount /dev/vdb /home/ubuntu/app/data
echo "Set user permission for the user 'ubuntu'"
chown -R ubuntu.users /home/ubuntu/app
echo "Starting Celery worker"
cd /home/ubuntu/app && sleep 25 && sudo -u ubuntu celery worker -A worker -l info
echo "System fully operational!"
