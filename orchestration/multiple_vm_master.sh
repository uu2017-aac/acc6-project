#!/bin/sh
echo "Setting up master!"
# Key added by cloud-init and heat template
echo "Updating the packages..."
apt-get update -y
apt-get upgrade -y
echo "Installing needed packages..."
apt-get install -y python-pip rabbitmq-server
pip install flask celery flower
systemctl start rabbitmq-server
echo "Setting RabbitMQ permissions..."
# benchop_worker:LetsBenchmark!@localhost/benchop'
rabbitmqctl add_user benchop_worker LetsBenchmark!
rabbitmqctl add_vhost benchop
rabbitmqctl set_user_tags benchop_worker benchop
rabbitmqctl set_permissions -p benchop benchop_worker ".*" ".*" ".*"
echo "Adding the SSH private key for connection to slaves..."
echo "PRIVATE_KEY" > /home/ubuntu/.ssh/id_rsa
echo "PUBLIC_KEY" > /home/ubuntu/.ssh/id_rsa.pub
echo "System setup done!"
echo "Fetching the data"
mkdir -p /home/ubuntu/app
wget https://www.dropbox.com/s/nc7rskzcuhv9kmt/benchop_aas.tar.xz -O \
    /home/ubuntu/app/benchop_aas.tar.xz
echo "Preparing the code"
cd /home/ubuntu/app && tar xvf benchop_aas.tar.xz
#echo "Mounting the data volume"
#mkdir -p /home/ubuntu/app/data
#mount /dev/vdb /home/ubuntu/app/data
echo "Set user permission for the user 'ubuntu'"
chown -R ubuntu.users /home/ubuntu/app
echo "Starting Flower interface"
cd /home/ubuntu/app && celery flower -A worker --port=8080 &
echo "Starting the Flask app"
python /home/ubuntu/app/webapp.py &
echo "System fully operational!"
