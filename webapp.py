from flask import Flask, jsonify, request, render_template, url_for
from time import sleep

import worker

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def setting_page():
    if request.method == 'POST':
        methods = request.form.getlist('method')
        problems = request.form.getlist('problem')
        style = request.form.get('style')
        celery_request = worker.method_tester(methods=methods,
                                              problems=problems,
                                              style=style)
        while not celery_request.ready():
            sleep(1)
        results = celery_request.get()
        if style == 'html':
            # Extract desired information
            results = '<table' + results.split('table')[1] + 'table>'
        return render_template('results.html', results=results, style=style)
    else:
        return render_template('selection.html',
                               methods=worker.methods,
                               problems=worker.problems
                               )


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
