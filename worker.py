from celery import Celery, group, chain, chord
from os import listdir, sep
from os.path import dirname, isdir, isfile, join, realpath
from subprocess import check_output
from tempfile import NamedTemporaryFile
from os import remove

"""
The URL used by Celery to connect to the RabbitMQ message service.
"""
rabbitmq_url = 'benchop_worker:LetsBenchmark!@localhost/benchop'
#rabbitmq_url = 'localhost:32769'

"""
The BENCHOP main directory to search within.
"""
benchop_dir = join(dirname(realpath(__file__)), 'BENCHOP')

"""
The available methods.
"""
methods = [d for d in listdir(benchop_dir) if isdir(join(benchop_dir, d))]
# All methods should be
#methods = ['MC','MC-S','QMC-S','MLMC','MLMC-A','FFT','FGL','COS','FD','FD-NU','FD-AD','RBF','RBF-FD','RBF-PUM','RBF-LSML','RBF-AD','RBF-MLT']

"""
The solvable problems.
"""
problems = ['1a', '1b', '1c', '2a', '2b', '2c']

"""
The problem solvers file pattern.
The key refers to the problem, while the value is the prefix of the specific
MatLab script to call to solve the problem.
"""
problems_files = {
    "1a": "BSeuCallUI_",
    "1b": "BSamPutUI_",
    "1c": "BSupoutCallI_",
    "2a": "BSeuCallUII_",
    "2b": "BSamPutUII_",
    "2c": "BSupoutCallII_"
}

"""
The data for each problem. Used to generate a proper Matlab script to run.
Extracted from the script `Table.m`.
"""
problems_data = {
    "1a": """S=[90,100,110]; K=100; T=1.0; r=0.03; sig=0.15;
U=[2.758443856146076 7.485087593912603 14.702019669720769];
par={S,K,T,r,sig};""",
    "1b": """S=[90,100,110]; K=100; T=1.0; r=0.03; sig=0.15;
U=[10.726486710094511 4.820608184813253 1.828207584020458];
par={S,K,T,r,sig};""",
    "1c": """S=[90,100,110]; K=100; T=1.0; r=0.03; sig=0.15; B=1.25*K;
U=[1.822512255945242 3.294086516281595 3.221591131246868];
par={S,K,T,r,sig,B};""",
    "2a": """S=[97,98,99]; sig=0.01; r=0.1; T=0.25; K=100;
U=[0.033913177006141, 0.512978189232598, 1.469203342553328];
par={S,K,T,r,sig};""",
    "2b": """S=[97,98,99]; K=100; T=0.25; r=0.1; sig=0.01;
U=[3.000000000000682, 2.000000000010786, 1.000000000010715];
par={S,K,T,r,sig};""",
    "2c": """S=[97,98,99]; sig=0.01; r=0.1; T=0.25; K=100; B=1.25*K;
U=[0.033913177006134, 0.512978189232598, 1.469203342553328];
par={S,K,T,r,sig,B};"""
}

"""
The running Celery app.
"""
app = Celery(
    'worker',
    backend='amqp://' + rabbitmq_url,
    broker='amqp://' + rabbitmq_url
)


@app.task
def call_octave(method, problem, solver):
    """
    The princess is in another castle!
    For now a dummy method which has a proper datatype
    """
    path = join(benchop_dir, method, solver)
    mathlab_script = 'rootpath=\'{}\';\n'.format(benchop_dir)
    mathlab_script += problems_data[problem]
    mathlab_script += 'solvers = {\'' + path  + '\'}'
    mathlab_script += """
[time, relativeError] = executor(rootpath, solvers, U, par);
display(time); display(relativeError);
"""
    mathlab_file = NamedTemporaryFile(delete=False)
    mathlab_file.write(mathlab_script.encode())
    mathlab_file.close()
    output = check_output(['octave', '-p', benchop_dir, mathlab_file.name])
    try:
        remove(mathlab_file.name)
    except FileNotFoundError:
        pass
    try:
        data = [s.split('=')[1].strip() for s in output.decode().split('\n')[-3:-1]]
    except IndexError:
        # Old version of Octave doesn't show the variables names...
        # Manage the data as the server wants
        data = [s.strip() for s in output.decode().split('\n')[-5:-2]]
        data = [data[0], data[2]]
    result = '{} ({})'.format(data[0], data[1])
    return {
        "method": method,
        "problem": problem,
        "result": result,
        # Should be needed but still
        "path": path,
        "solver": solver
        }

@app.task
def merger(results):
    """
    Celery task to merge all the obtained results to a table.

    Parameters:
    * results: the pronouns counters to merge
    """
    result = {}
    for r in results:
        if r['method'] not in result:
            result[r['method']] =  {}
        result[r['method']][r['problem']] = r['result']

    return result


@app.task
def elaborate_output(table, style='python'):
    """
    Celery task to convert the table to the desired output format.

    Parameters:
    * style: set the output style. The admitted values are
      - 'python': plain Python output, to be used in script
      - 'latex':  LaTeX-formatted table, within a string
      - 'html':   HTML-formatted table, withing a string
    """
    problems_keys = sorted(table[list(table.keys())[0]].keys())
    for method in table:
        values_line = [method]
        for p in problems_keys:
            if p not in table[method]:
                table[method][p] = 'Miss'
    values = [
        [method] + [
            table[method][problem] for problem in problems_keys
            ] for method in table
    ]
    if style == 'latex':
        # Header formatting
        header = """\documentclass[a4paper,12pt]{article}

\\usepackage{fullpage}
\\usepackage{rotating}
\\begin{document}

\\begin{table}[h!]
\centering
\\begin{sideways}
\\begin{tabular}{c|c|c|c|c|c|c}
 & """
        header += ' & '.join(problems_keys)
        header += ' \\\\\n'
        # Content formatting
        content = '\hline '
        for line in values:
            content += ' & '.join(line)
            content += ' \\\\\n'
        return  header + content + """\\end{tabular}
\\end{sideways}
\\caption{BENCHOP Comparison Table}
\\label{table:BenchopTable}

\\end{table}
\\end{document}"""
    elif style == 'html':
        header = """<html>
<head>
    <title>BENCHOP-AAS Results</title>
</head>
<body>
    <h2>BENCHOP-AAS Results</h2>
    <table style="width:100%">
    <tr>
        <th></th>
"""
        for p in problems_keys:
            header += '        <th>{}</th>\n'.format(p)
        header += '    </tr>\n'
        content = ''
        for line in values:
            content += '    <tr>\n'
            for value in line:
                content += '        <td>{}</td>\n'.format(value)
            content += '    </tr>\n'
        return header + content + '    </table>\n</body>\n</html>'
    elif style == 'python':
        return table
    else:
        raise TypeError(
            'Unsupported output type: ', style,
            "supported ones: 'latex', 'html', 'python'"
            )


@app.task
def method_tester(methods=methods, problems=problems, style='python'):
    """
    Celery main task, which groups all the subtasks in the proper order.

    Parameters:
    * methods: the methods to use for finding solutions.
    * problems: the problems to solve
    * style: the style of the desired output.
    See: all the tasks in the module.
    """
    # Chainining has problems, refer using link!
    c = group(
        call_octave.s(i, j, k) for i, j, k in list_solvers(methods, problems)
        ) | merger.s() | elaborate_output.s(style=style)
    return c()


def refresh_methods():
    """
    Refresh the available methods.
    """
    # Changing the global variable
    global methods
    methods = [d for d in listdir(benchop_dir) if isdir(join(benchop_dir, d))]


def list_solvers(methods=methods, problems=problems):
    """
    Return the available solvers.

    Parameters:
    * methods: the methods to use for finding solutions.
    * problems: the problems to solve.
    """
    selected_problems = {
        p: problems_files[p]
        for p in problems_files
        if p in problems
    }
    for method in methods:
        method_dir = join(benchop_dir, method)
        for f in listdir(method_dir):
            # If they are files
            if isfile(join(method_dir, f)) and f.endswith('.m') \
            and f[:f.find('_') + 1] in selected_problems.values():
                # We assume that a solver works only for one problem!
                problem = [
                    key for key in selected_problems
                    if selected_problems[key] == f[:f.find('_') + 1]
                ][0]
                yield(method, problem, f)

if __name__ == '__main__':
    app.worker_main()
