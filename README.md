## Notes for the project
Add here useful notes for the project, please~

### Octave Docker container
A useful Docker container for some experiments can be run with
`docker run --rm -it -v "$(pwd | tr A-Z a-z)":/source schickling/octave`

### Accessing worker API
In order to call the methods one can use
`worker.method_tester()`
or manually use one of the tuple of
`worker.list_solvers()`
and have this workflow
`worker.call_octave.s(unpacked_tuple) | worker.merger.s() | worker.elaborate_output.s(style='mystyle')` 